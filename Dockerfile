# Choose the Node.js version
FROM node:18

# Set the working directory
WORKDIR /src

# Copy package.json and package-lock.json
COPY package.json yarn.lock ./

# Install dependencies
RUN yarn install

# Copy the rest of your app's source code
COPY . .

# Build the app
RUN yarn build

# Expose the port your app runs on
EXPOSE 3000

# Start the app
CMD [ "yarn", "start" ]
