
# Project Documentation

## Introduction
This project is a simplified post management application. It allows users to view a list of posts, reorder them, and time-travel to previous states of the application. The project is built using React, TypeScript, Tailwind CSS, and Framer Motion for animations.

## Demo
Here is the live demo of the app https://temper-task-d1mcjcwv4-saeedh582-gmailcom.vercel.app/

## Installation
1. Clone the repository to your local machine.
```bash
git clone https://gitlab.com/saeed8697/temper.git
```
2. Navigate to the project directory.
```bash
cd temper
```
3. Install the dependencies.
```bash
npm install
```
or
```bash
yarn install
```
4. Start the development server.
```bash
npm start
```
or
```bash
yarn start
```
for testing
```bash
yarn test
```
The application will now be running on `http://localhost:3000`.

## Build and run with Docker

for building project firs run 
```bash
docker build -t temper-task-app .
```


then run this
```bash
docker run -p 3000:3000 -d temper-task-app
```




## Directory Structure
- `src/`
  - `atoms/`: Contains the smallest building blocks of the project like Button and ActionButton.
  - `molecules/`: Slightly complex components composed of atoms like PostItem and ActionItem.
  - `organisms/`: More complex components composed of molecules and/or atoms like PostList and ActionList.
  - `context/`: Contains the context providers used in the project.
  - `hooks/`: Custom hooks.
  - `services/`: Services like API calls.
  - `types/`: Type definitions and interfaces.
  - `utils/`: Utility functions and constants.
  - `App.tsx`: The root component of the project.
  - `index.tsx`: The entry point of the project.

## Components
### Atoms
- `Button`: A reusable button component.
- `ActionButton`: A button specifically for the "Time Travel" action.

### Molecules
- `PostItem`: Represents a single post item in the list.
- `ActionItem`: Represents a single action item in the list.

### Organisms
- `PostList`: Displays the list of posts.
- `ActionList`: Displays the list of actions.

## Hooks
- `useFetchPosts`: Custom hook to fetch posts from the API.
- `usePostReducer`: Custom hook for state management.

## Context
- `PostContext`: Provides state and dispatch function to child components.

## Utilities
- `actions.helper.ts`: Contains helper functions for handling actions like `handleMoveAction`, `handleSetInitialPosts`, and `handleTimeTravel`.

## Types
- `action.types.ts`: Contains the type definitions related to actions.
- `post.types.ts`: Contains the type definitions related to posts.

## Services
- `fetchPosts.api.ts`: Contains the function to fetch posts from the API.

## Tests
- Unit tests and end-to-end tests are located in the `__tests__` directory.

---

