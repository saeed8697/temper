// src/contexts/PostContext.tsx
import React, {createContext, ReactNode, useContext, useReducer} from 'react';
import usePostReducer from "../hooks/usePostReducer";
import {Action, State} from "../types/action.types";

// Interface for props passed to the PostProvider component
interface PostContextProps {
    children: ReactNode;
}

// Interface for the value provided by the PostContext
interface PostContextValue {
    state: State;
    dispatch: React.Dispatch<Action>;
}

// Initial state
const initialState: State = {posts: [], actions: [], initialPosts: []};

/**
 * Custom hook to provide a way for components to access the PostContext.
 * Throws an error if used outside of a PostProvider.
 *
 * @returns The value of the PostContext.
 */
export const usePostContext = (): PostContextValue => {
    const context = useContext(PostContext);
    if (!context) {
        throw new Error('usePostContext must be used within a PostProvider');
    }
    return context;
};

export const PostContext = createContext<PostContextValue | null>(null);

/**
 * Custom hook to provide a way for components to access the PostContext.
 * Throws an error if used outside of a PostProvider.
 *
 * @returns The value of the PostContext.
 */
const PostProvider: React.FC<PostContextProps> = ({children}) => {
    const [state, dispatch] = useReducer(usePostReducer, initialState);

    return (
        <PostContext.Provider value={{state, dispatch}}>
            {children}
        </PostContext.Provider>
    );
};

export default PostProvider;
