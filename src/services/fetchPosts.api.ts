import {Post} from "../types/post.types";

// Endpoint URL for fetching posts
const API_ENDPOINT = 'https://jsonplaceholder.typicode.com';

/**
 * Fetches posts from the external API endpoint.
 * This function sends an HTTP request to the API endpoint, handles the response,
 * and returns the first five posts.
 *
 * @returns A promise that resolves to an array of Post objects.
 * @throws Will throw an error if the network response is not ok.
 */
export const fetchPosts = async (): Promise<Post[]> => {
    const response = await fetch(`${API_ENDPOINT}/posts`);
    if (!response.ok) {
        throw new Error('Network response was not ok ' + response.statusText);
    }
    const data: Post[] = await response.json();
    return data.slice(0, 5);  // Only take the first 5 posts
};
