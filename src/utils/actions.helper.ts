import {Action, MOVE_ACTION, MoveAction, SET_INITIAL_POSTS, State, TIME_TRAVEL} from "../types/action.types";
import {Post} from "../types/post.types";
import React from "react";
import uuid from 'react-uuid';

/**
 * Handles the action of moving a post from one index to another.
 * Creates a new state with the updated order of posts and a new action logged.
 *
 * @param state - The current state.
 * @param payload - The action payload containing oldIndex and newIndex.
 * @returns The updated state.
 */
export const handleMoveAction = (state: State, payload: { oldIndex: number, newIndex: number }): State => {
    const updatedPosts = [...state.posts];
    const [movedPost] = updatedPosts.splice(payload.oldIndex, 1);
    updatedPosts.splice(payload.newIndex, 0, movedPost);
    const updatedActions: MoveAction = {
        type: MOVE_ACTION,
        payload,
        description: `Moved Post ${movedPost.id} from index ${payload.oldIndex} to index ${payload.newIndex}`,
        id: uuid(),
    };
    return {...state, posts: updatedPosts, actions: [updatedActions, ...state.actions]};
};

/**
 * Sets the initial state of posts.
 *
 * @param state - The current state.
 * @param posts - The initial list of posts.
 * @returns The updated state with the new list of posts.
 */
export const handleSetInitialPosts = (state: State, posts: Post[]): State => {
    return {...state, posts, initialPosts: posts};
};

/**
 * Handles the action of time traveling to a previous state.
 * Reverts the order of posts to a previous state based on the actions logged.
 *
 * @param state - The current state.
 * @param index - The index of the action to revert to.
 * @returns The updated state reverted to the specified action.
 */
export const handleTimeTravel = (state: State, index: number): State => {
    const revertedPosts = [...state.posts];
    for (let i = 0; i <= index; i++) {
        const actionPayload = state.actions[i];
        if (state.actions[i].type === MOVE_ACTION) {
            const {payload} = actionPayload as MoveAction;
            const [movedPost] = revertedPosts.splice(payload.newIndex, 1);
            revertedPosts.splice(payload.oldIndex, 0, movedPost);
        }
    }
    const updatedActions = state.actions.slice(index + 1);
    return {...state, posts: revertedPosts, actions: updatedActions};
};

export const timeTravelDispatch = (index: number, dispatch: React.Dispatch<Action>) => {
    try {
        dispatch({type: TIME_TRAVEL, payload: {index}});
    } catch (error) {
        console.error('Error in timeTravel:', error);
    }
};

export const movePostDispatch = (oldIndex: number, newIndex: number, dispatch: React.Dispatch<Action>) => {
    try {
        dispatch({type: MOVE_ACTION, payload: {oldIndex, newIndex}});
    } catch (error) {
        console.error('Error in move action dispatch:', error);
    }
};
export const initialPostDispatch = (posts: Post[], dispatch: React.Dispatch<Action>) => {
    try {
        dispatch({type: SET_INITIAL_POSTS, payload: {posts: posts}});
    } catch (error) {
        console.error('Error in setting initials dispatch:', error);
    }
};