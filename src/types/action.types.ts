import {Post} from "./post.types";
export const MOVE_ACTION = 'MOVE_ACTION';
export const SET_INITIAL_POSTS = 'SET_INITIAL_POSTS';
export const TIME_TRAVEL = 'TIME_TRAVEL';
export interface MoveAction {
    id?:string
    type: typeof MOVE_ACTION;
    payload: {
        oldIndex: number;
        newIndex: number;
    };
    description?: string
}

export interface TimeTravelAction {
    id?:string

    type: typeof TIME_TRAVEL;
    payload: {
        index: number;
    };
    description?: string

}

export interface SetInitialPostsAction {
    id?:string
    type: typeof SET_INITIAL_POSTS;
    payload: {
        posts: Post[];
    };
    description?: string

}

export type Action = MoveAction | TimeTravelAction | SetInitialPostsAction;

export interface State {
    posts: Post[];
    actions: Action[];
    initialPosts: Post[];

}