import React, {useEffect} from 'react';
import useFetchPosts from "./hooks/useFetchPosts";
import {usePostContext} from "./context/PostContext";
import PostList from "./components/organisms/postList";
import {initialPostDispatch} from "./utils/actions.helper";
import Error from "./components/atoms/Error";
import Loading from "./components/atoms/Loaing";
import ActionList from "./components/organisms/ActionList";


const App: React.FC = () => {
    const {dispatch} = usePostContext();
    const [posts, loading, error] = useFetchPosts();

    useEffect(() => {
        initialPostDispatch(posts, dispatch)
    }, [dispatch, posts])

    return (
        <div className="min-h-screen flex flex-col md:flex-row">
            {loading ? (<Loading/>) : (
                <>
                    <div className="flex-1 p-8 m-9">
                        <h1 className="text-2xl text-white font-bold mb-4">Sortable Post List</h1>
                        <PostList/>
                    </div>
                    <div className="flex-1 p-8 m-5 border-t md:border-t-0 ">
                        <div className='bg-white rounded'>
                            <h1 className="text-xl font-bold  p-5 ">List of Actions Committed</h1>
                            <ActionList/>
                        </div>
                    </div>
                </>
                )
            }

            {error && <Error message={(error as Error).message}/>}

        </div>
    )
        ;
};

export default App;
