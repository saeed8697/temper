import { useState, useEffect } from 'react';
import {Post} from "../types/post.types";
import {fetchPosts} from "../services/fetchPosts.api";

/**
 * Custom hook to fetch posts from an external source.
 * This hook maintains and returns the state of posts, loading status, and any errors encountered during the fetching process.
 *
 * @returns An array containing:
 * - A list of posts fetched.
 * - A boolean indicating whether the fetching process is ongoing.
 * - An error object if an error occurred, or null if no errors occurred.
 */
const useFetchPosts = (): [Post[], boolean, Error | null] => {
    const [posts, setPosts] = useState<Post[]>([]);
    const [loading, setLoading] = useState<boolean>(true);
    const [error, setError] = useState<Error | null>(null);

    useEffect(() => {
        const getPosts = async () => {
            try {
                const data = await fetchPosts();
                setPosts(data);
            } catch (error) {
                setError(error as Error);
            } finally {
                setLoading(false);
            }
        };

        getPosts();
    }, []);

    return [posts, loading, error];
};

export default useFetchPosts;
