import {Action, MOVE_ACTION, SET_INITIAL_POSTS, State, TIME_TRAVEL} from "../types/action.types";
import {handleMoveAction, handleSetInitialPosts, handleTimeTravel} from "../utils/actions.helper";

/**
 * Reducer function to handle actions related to posts and actions.
 * Delegates the handling of different action types to specific helper functions.
 *
 * @param state - The current state.
 * @param action - The action to be handled.
 * @returns The updated state based on the action type.
 */
const usePostReducer = (state: State, action: Action): State => {
    switch (action.type) {
        case MOVE_ACTION:
            return handleMoveAction(state, action.payload);
        case SET_INITIAL_POSTS:
            return handleSetInitialPosts(state, action.payload.posts);
        case TIME_TRAVEL:
            return handleTimeTravel(state, action.payload.index);
        default:
            throw new Error(`Unhandled action type`);
    }
};
export default usePostReducer;
