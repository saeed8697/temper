import React from 'react';
import {motion} from 'framer-motion';
import Button from '../atoms/Button';
import {Action} from "../../types/action.types";
import {usePostContext} from "../../context/PostContext";

interface PostItemProps {
    postId: number;
    index: number;
    onMove: (oldIndex: number, newIndex: number, dispatch: React.Dispatch<Action>) => void;  // Callback to be invoked when a move button is clicked
    isMoveUpDisabled: boolean;  // Boolean to control the visibility of the move up button
    isMoveDownDisabled: boolean;  // Boolean to control the visibility of the move down button
}


/**
 * A component representing an individual post item.
 * It contains a title, and two buttons to move the post up or down.
 *
 * @param postId - The ID of the post.
 * @param index - The index of the post in the list.
 * @param onMove - The callback function to be invoked when a move button is clicked.
 * @param isMoveUpDisabled - Boolean to control the visibility of the move up button.
 * @param isMoveDownDisabled - Boolean to control the visibility of the move down button.
 * @returns The rendered PostItem component.
 */
const PostItem: React.FC<PostItemProps> = ({postId, index, onMove, isMoveUpDisabled, isMoveDownDisabled}) => {
    const {dispatch} = usePostContext();

    return (
        <motion.div
            data-testid="post-item"
            className="flex items-center rounded justify-between p-4 border rounded h-24 bg-white"
            layout
        >
            <h2 data-testid="post-item-text" className="text-lg font-semibold">Post {postId}</h2>
            <div className='flex flex-col'>
                <Button
                    icon={
                        <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M8 4L4 8H12L8 4Z" fill="black"/>
                        </svg>
                    }
                    onClick={() => onMove(index, index - 1, dispatch)}
                    hidden={isMoveUpDisabled}
                    testid={`up-button-${postId}`}
                    className="p-2 rounded-full disabled:opacity-50"
                />
                <Button
                    icon={
                        <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M8 12L12 8L4 8L8 12Z" fill="black"/>
                        </svg>
                    }
                    onClick={() => onMove(index, index + 1, dispatch)}
                    hidden={isMoveDownDisabled}
                    testid={`down-button-${postId}`}
                    className="p-2 rounded-full disabled:opacity-50"
                />
            </div>
        </motion.div>
    );
};

export default PostItem;
