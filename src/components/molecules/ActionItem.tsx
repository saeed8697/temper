import React from 'react';
import {motion} from 'framer-motion';
import ActionButton from '../atoms/ActionButton';

interface ActionItemProps {
    description?: string;
    onTimeTravel: () => void;
}

/**
 * A component representing an individual action item.
 * It contains a description and an action button to trigger a time travel event.
 *
 * @param description - The description of the action.
 * @param onTimeTravel - The callback function to be invoked when the action button is clicked.
 * @returns The rendered ActionItem component.
 */
const ActionItem: React.FC<ActionItemProps> = ({description, onTimeTravel}) => {
    return (
        <motion.div
            initial={{opacity: 0, y: -10}}
            animate={{opacity: 1, y: 0}}
            exit={{opacity: 0, y: -10}}
            className="flex items-center justify-between py-2 border-b p-3 bg-white rounded"
        >
            <p data-testid="action-description">{description}</p>
            <ActionButton onClick={onTimeTravel} testId='time-travel-button'/>
        </motion.div>
    );
};

export default ActionItem;
