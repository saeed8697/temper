import React from 'react';
import {usePostContext} from "../../context/PostContext";
import {AnimatePresence} from 'framer-motion';
import ActionItem from '../molecules/ActionItem';
import {timeTravelDispatch} from "../../utils/actions.helper";

/**
 * The ActionList component renders a list of actions, allowing users to perform time travel.
 * Each action is represented by an ActionItem component.
 */
const ActionList: React.FC = () => {
    const {state, dispatch} = usePostContext();

    return (
        <div className="bg-gray-100 rounded p-7">
            {!state.actions.length && <div className='text-center'> No record </div>}
            <AnimatePresence>
                {state.actions.map((action, index) => (
                    <ActionItem
                        key={action.id}
                        description={action.description}
                        onTimeTravel={() => timeTravelDispatch(index, dispatch)}
                    />
                ))}
            </AnimatePresence>
        </div>
    );
};

export default ActionList;
