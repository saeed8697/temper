import React from 'react';
import { usePostContext } from "../../context/PostContext";
import { motion } from 'framer-motion';
import PostItem from '../molecules/PostItem';
import {movePostDispatch} from "../../utils/actions.helper";

const PostList: React.FC = () => {
    const { state} = usePostContext();


    return (
        <motion.div
            data-testid="post-list"
            className="rounded space-y-4 flex flex-col h-full"
            layout
        >
            {state.posts.map((post, index) => (
                <PostItem
                    key={post.id}
                    postId={post.id}
                    index={index}
                    onMove={movePostDispatch}
                    isMoveUpDisabled={index === 0}
                    isMoveDownDisabled={index === state.posts.length - 1}
                />
            ))}
        </motion.div>
    );
};

export default PostList;
