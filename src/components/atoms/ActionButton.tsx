import React from 'react';

interface ActionButtonProps {
    onClick: () => void;
    testId?: string
}

/**
 * A reusable button component to trigger certain actions.
 *
 * @param onClick - The callback function to be invoked when the button is clicked.
 * @param testId
 * @returns The rendered button component.
 */
const ActionButton: React.FC<ActionButtonProps> = ({onClick, testId}) => {
    return (
        <button
            data-testid={testId}
            onClick={onClick}
            className="p-3 rounded bg-green-500 hover:bg-green-600 text-green-950"
        >
            Time Travel
        </button>
    );
};

export default ActionButton;
