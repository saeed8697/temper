import React from 'react';

interface ErrorProps {
    message: string;
}

const Error: React.FC<ErrorProps> = ({ message }) => {
    return (
        <div className="bg-orange-100 border-l-4 border-orange-500 text-orange-700 p-4 absolute right-0 bottom-0 mr-5 mb-5" role="alert">
            <p className="font-bold">Error</p>
            <p>{message}</p>
        </div>
    );
};

export default Error;
