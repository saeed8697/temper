import React from 'react';

interface ButtonProps {
    icon: React.ReactNode;
    onClick: () => void;
    hidden?: boolean;
    className?: string;
    testid?: string;
}

/**
 * A reusable button component with an icon.
 *
 * @param icon - The icon to be displayed within the button.
 * @param onClick - The callback function to be invoked when the button is clicked.
 * @param hidden - Boolean to control the visibility of the button.
 * @param className - Additional CSS classes to be applied to the button.
 * @param testid
 * @returns The rendered button component.
 */
const Button: React.FC<ButtonProps> = ({ icon, onClick, hidden, className,testid }) => {
    return (
        <button onClick={onClick} hidden={hidden} className={className} data-testid={testid}>
            {icon}
        </button>
    );
};

export default Button;
