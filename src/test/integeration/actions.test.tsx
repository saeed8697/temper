import React, {ReactNode, useReducer} from 'react';
import {fireEvent, render, screen, waitFor} from '@testing-library/react';
import {PostContext} from "../../context/PostContext";
import PostList from "../../components/organisms/postList";
import ActionList from "../../components/organisms/ActionList";
import usePostReducer from "../../hooks/usePostReducer";

// Mock data
const mockPosts = [{id: 1, title: 'Post 1', body: 'Body 1'}, {id: 2, title: 'Post 2', body: 'Body 2'}, {
    id: 3,
    title: 'Post 3',
    body: 'Body 3'
}, {id: 4, title: 'Post 4', body: 'Body 4'}, {id: 5, title: 'Post 5', body: 'Body 5'},];

// Custom provider for testing
const TestProvider: React.FC<{ children: ReactNode }> = ({ children }) => {
    const [state, dispatch] = useReducer(usePostReducer, {posts: mockPosts, actions: [], initialPosts: []});

    return (<PostContext.Provider value={{state: state, dispatch: dispatch}}>
            {children}
        </PostContext.Provider>);
};


describe('Reorder Test', () => {
    test('reordering posts and logging actions', () => {
        render(<TestProvider>
            <PostList/>
            <ActionList/>
        </TestProvider>);

        // Assume the down button for the first post has a data-testid of 'down-button-0'
        const downButton = screen.getByTestId('down-button-1');
        fireEvent.click(downButton);

        // Check that the posts have been reordered
        const posts = screen.getAllByTestId('post-item-text');
        expect(posts[0]).toHaveTextContent('Post 2');
        expect(posts[1]).toHaveTextContent('Post 1');

        // Check that an action has been logged
        // Assume the action description has a data-testid of 'action-description'
        const actionDescription = screen.getByTestId('action-description');
        expect(actionDescription).toHaveTextContent('Moved Post 1 from index 0 to index 1');
    });
});

describe('Time Travel Test',  () => {
    test('time traveling to a previous state and removing actions', async () => {
        render(<TestProvider>
            <PostList/>
            <ActionList/>
        </TestProvider>);

        // Simulate reordering posts
        const downButton = screen.getByTestId('down-button-1');
        fireEvent.click(downButton);


        // Simulate time traveling
        // Assume the time travel button has a data-testid of 'time-travel-button'
        const timeTravelButton = screen.getByTestId('time-travel-button');
        fireEvent.click(timeTravelButton);

        // Wait for the action-description elements to be removed from the DOM
        await waitFor(() => {
            const actionItems = screen.queryAllByTestId('action-description');
            expect(actionItems).toHaveLength(0);
        });

        // Check that the posts have reverted to the original order
        const posts = screen.getAllByTestId('post-item-text');
        expect(posts[0]).toHaveTextContent('Post 1');
        expect(posts[1]).toHaveTextContent('Post 2');


    });
});
