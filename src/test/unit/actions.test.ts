import {MOVE_ACTION, State} from "../../types/action.types";
import {Post} from "../../types/post.types";
import {handleMoveAction, handleSetInitialPosts, handleTimeTravel} from "../../utils/actions.helper";

describe('Actions Handlers', () => {
    const mockState: State = {
        posts: [
            { id: 1, title: 'Post 1', body: 'Body 1' },
            { id: 2, title: 'Post 2', body: 'Body 2' },
            { id: 3, title: 'Post 3', body: 'Body 3' },
        ],
        actions: [],
        initialPosts: []
    };

    it('should handle move action', () => {
        const payload = { oldIndex: 0, newIndex: 2 };
        const newState = handleMoveAction(mockState, payload);

        expect(newState.posts[2]).toEqual({ id: 1, title: 'Post 1', body: 'Body 1' });
        expect(newState.actions[0].type).toEqual(MOVE_ACTION);
        expect(newState.actions[0].payload).toEqual(payload);
    });

    it('should set initial posts', () => {
        const newPosts: Post[] = [
            { id: 4, title: 'Post 4', body: 'Body 4' },
            { id: 5, title: 'Post 5', body: 'Body 5' },
        ];
        const newState = handleSetInitialPosts(mockState, newPosts);

        expect(newState.posts).toEqual(newPosts);
        expect(newState.initialPosts).toEqual(newPosts);
    });

    it('should handle time travel', () => {
        const movePayload = { oldIndex: 0, newIndex: 2 };
        const movedState = handleMoveAction(mockState, movePayload);
        const newState = handleTimeTravel(movedState, 0);

        expect(newState.posts[0]).toEqual({ id: 1, title: 'Post 1', body: 'Body 1' });
        expect(newState.actions.length).toEqual(0);
    });
});
