// src/__tests__/useFetchPosts.test.js
import useFetchPosts from '../../hooks/useFetchPosts';
import {renderHook, waitFor} from "@testing-library/react";

jest.mock('node-fetch', () => jest.fn());

test('fetches posts', async () => {
    const { result } = renderHook(() => useFetchPosts());
    await waitFor(() => {
        expect(result.current[0].length).toBe(5);
    })
});
